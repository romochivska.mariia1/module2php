<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Your Laravel App</title>
    <link rel="stylesheet" href="<?php echo e(asset('css/app.css')); ?>">
</head>
<body>
<h1 class="text-3xl font-bold mb-6 text-green-600">Manufacturers List</h1>

<?php $__currentLoopData = $manufacturers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $manufacturer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <div class="bg-green-200 p-6 mb-6 rounded-md shadow-lg">
        <p class="text-lg text-green-800"><?php echo e($manufacturer->brand); ?>, <?php echo e($manufacturer->country); ?>, <?php echo e($manufacturer->contact_phone); ?>, <?php echo e($manufacturer->email); ?></p>
    </div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php echo $__env->yieldContent('content'); ?>
</body>
</html>
<?php /**PATH C:\xampp\htdocs\Module2\laravel\resources\views/manufacturers/index.blade.php ENDPATH**/ ?>