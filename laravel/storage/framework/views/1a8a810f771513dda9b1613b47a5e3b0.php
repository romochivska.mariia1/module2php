<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Your Laravel App</title>
    <link rel="stylesheet" href="<?php echo e(asset('css/app.css')); ?>">
</head>
<body>

<h1 >Add New Manufacturer</h1>

<form method="POST" action="<?php echo e(route('manufacturers.store')); ?>" >
    <?php echo csrf_field(); ?>

    <div >
        <label for="brand">Brand:</label>
        <input type="text" name="brand" required >
    </div>

    <div class="mb-4">
        <label for="country" class="block text-sm font-medium text-green-600">Country:</label>
        <input type="text" name="country" required >
    </div>

    <div class="mb-4">
        <label for="contact_phone" class="block text-sm font-medium text-green-600">Contact Phone:</label>
        <input type="text" name="contact_phone" required>
    </div>

    <div class="mb-4">
        <label for="email" class="block text-sm font-medium text-green-600">Email:</label>
        <input type="email" name="email" required >
    </div>

    <button type="submit">Add Manufacturer</button>
</form>

<?php echo $__env->yieldContent('content'); ?>
</body>
</html>
<?php /**PATH C:\xampp\htdocs\Module2\laravel\resources\views/manufacturers/create.blade.php ENDPATH**/ ?>