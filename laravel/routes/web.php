<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ManufacturerController;


Route::get('/', [ManufacturerController::class, 'index'])->name('manufacturers.index');
Route::get('/create', [ManufacturerController::class, 'create'])->name('manufacturers.create');
Route::post('/', [ManufacturerController::class, 'store'])->name('manufacturers.store');
