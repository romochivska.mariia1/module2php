<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Your Laravel App</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>
<body>
<h1 class="text-3xl font-bold mb-6 text-green-600">Manufacturers List</h1>

@foreach ($manufacturers as $manufacturer)
    <div class="bg-green-200 p-6 mb-6 rounded-md shadow-lg">
        <p class="text-lg text-green-800">{{ $manufacturer->brand }}, {{ $manufacturer->country }}, {{ $manufacturer->contact_phone }}, {{ $manufacturer->email }}</p>
    </div>
@endforeach
@yield('content')
</body>
</html>
